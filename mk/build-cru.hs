import Data.Function {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}

import Sound.SC3.Common.Math {- hsc3 -}
import Sound.SC3.Common.Math.Operator {- hsc3 -}

import qualified Sound.SC3.UGen.DB as DB {- hsc3-db -}
import qualified Sound.SC3.UGen.DB.Bindings.Haskell as DB {- hsc3-db -}
import qualified Sound.SC3.UGen.DB.Record as DB {- hsc3-db -}
import qualified Sound.SC3.UGen.DB.Rename as DB {- hsc3-db -}

import Sound.SC3.UGen.Record.Common {- hsc3-rec -}

-- * CRU

-- > map DB.ugen_name ugen_db
ugen_db :: [DB.U]
ugen_db =
    let db = sortBy (compare `on` DB.ugen_name) DB.ugen_db
    in filter (\u -> DB.ugen_name u `elem` buildList) db

input_proc :: DB.U -> (String,Int) -> String
input_proc u (nm,ix) =
    case DB.input_enumeration u ix of
      Just _ -> nm
      Nothing -> "(ugen " ++ nm ++ ")"

rename_input :: String -> String
rename_input nm =
    let nm' = DB.rename_input nm
    in if nm' `elem` ["median","trig","index","lag","gate","decay"]
       then nm' ++ "_"
       else nm'

-- > putStrLn$unlines$ map cons ugen_db
cons :: DB.U -> String
cons u =
    let nm = DB.ugen_name u
        ty = DB.u_input_types "Ugen" u
        ip = map (rename_input . DB.input_name) (DB.ugen_inputs u)
        sg = map (\(i,t) -> i ++ " :: " ++ t) (zip ip ty)
        genId = False
        pre = catMaybes [if genId && DB.ugen_nondet u then Just "uid :: Char" else Nothing
                        ,if DB.u_requires_rate u then Just "rate :: Rate" else Nothing]
        rhs = pre ++ sg
    in DB.unwords_rm ("           |" : nm : "{" : (intersperse "," rhs ++ ["}"]))

-- > putStrLn$unlines$ map convert ugen_db
convert :: DB.U -> String
convert u =
    let nm = DB.ugen_name u
        nm' = "SC3." ++ DB.fromSC3Name nm
        ip = map (("_" ++) . DB.input_name) (DB.ugen_inputs u)
        ip' = map (input_proc u) (zip ip [0..])
        rt = if DB.u_requires_rate u then "_rate" else ""
        genId = False
        uid = if genId && DB.ugen_nondet u then "_uid" else ""
    in DB.unwords_rm ("     " : nm : uid : rt : ip ++ ["->",nm',uid,rt] ++ ip')

-- > putStrLn$unlines$ concatMap default_u ugen_db
default_u :: DB.U -> [String]
default_u u =
    let nm = DB.ugen_name u
        nm' = DB.fromSC3Name nm
        df = map (real_pp 5 . DB.input_default) (DB.ugen_inputs u)
        rt = if DB.u_requires_rate u then show (DB.ugen_default_rate u) else ""
        genId = False
        uid = if genId && DB.ugen_nondet u then "'α'" else ""
    in [unwords [nm',"::","Ugen"]
       ,DB.unwords_rm ([nm',"=",nm,uid,rt] ++ df)]

-- > putStrLn$unlines$ concat unary
unary :: [[String]]
unary =
    let uop = map show [minBound::SC3_Unary_Op .. maxBound::SC3_Unary_Op]
        f nm = [DB.fromSC3Name nm ++ " :: Ugen -> Ugen"
               ,concat [DB.fromSC3Name nm," = Ugen . ",DB.fromSC3Name nm," . ugen"]]
    in map f uop
