import Data.Function {- base -}
import Data.List {- base -}
import System.FilePath {- filepath -}
import Text.Printf {- base -}

import qualified Sound.SC3.UGen.DB as DB {- hsc3-db -}
import qualified Sound.SC3.UGen.DB.Record as DB {- hsc3-db -}
import qualified Sound.SC3.UGen.DB.Rename as Rename {- hsc3-db -}

import Sound.SC3.UGen.Record.Common {- hsc3-rec -}

-- | List of field names and types.
--
-- > map (fmap inputs_of . DB.u_lookup_cs) ["MouseX","RLPF"]
inputs_of :: DB.U -> [(String, String)]
inputs_of u =
    let is = DB.ugen_inputs u
    in ("rate", "SC3.Rate") : zip (map DB.input_name is) (repeat "SC3.UGen")

-- | Append a character to each but the last string.
with_char :: Char -> [String] -> [String]
with_char c l =
    case l of
      x:y:xs -> (x ++ [c]) : with_char c (y:xs)
      _ -> l

with_comma :: [String] -> [String]
with_comma = with_char ','

with_space :: [String] -> [String]
with_space = with_char ' '

-- | Construct the parameter record.
gen_param :: (DB.U,DB.U) -> [String]
gen_param (u,u') =
    let n = DB.ugen_name u
        pre = [printf "data %s = %s {" n n]
        post = ["  } deriving (Show)"]
        f (nm, ty) = printf "  %s :: %s" nm ty
    in pre ++ with_comma (map f (inputs_of u')) ++ post

-- | Construct a default instance of the parameter structure.
gen_defaults :: (DB.U,DB.U) -> [String]
gen_defaults (u,u') =
    let nm = DB.ugen_name u
        nm' = Rename.fromSC3Name nm ++ "R"
        is' = DB.ugen_inputs u'
        pre = [ printf "%s :: %s" nm' nm
              , printf "%s = %s {" nm' nm]
        post = [ "  }" ]
        rt = [printf "  rate = SC3.%s," (show (DB.ugen_default_rate u))]
        f i = printf "  %s = %f" (DB.input_name i) (DB.input_default i)
    in pre ++ rt ++ with_comma (map f is') ++ post

-- | Generate a list of variable names (a,b..)
var_names :: Int -> [String]
var_names n = map (: "'") (take n ['a'..])

-- | Generate the constructor for the unit generator.
gen_cons :: DB.U -> [String]
gen_cons u =
    let n = DB.ugen_name u
        is = DB.ugen_inputs u
        o = DB.ugen_outputs u
        xs = var_names (length is)
        o' = case o of
               Just x -> show x
               Nothing -> "undefined"
    in [ printf "mk%s :: %s -> SC3.UGen" n n
       , printf "mk%s (%s %s %s) = SC3.mkOsc %s \"%s\" [%s] %s"
                n
                n
                "r"
                (concat (with_space xs))
                "r"
                n
                (concat (with_comma xs))
                o']

-- | Generate instance of the Make class.
gen_make :: DB.U -> [String]
gen_make u =
    let n = DB.ugen_name u
    in [ printf "instance Make %s where" n
       , printf "  ugen = mk%s" n ]

-- | Path to write files to (relative to mk)
sc3_ugen_dir :: FilePath
sc3_ugen_dir = ".." </> "Sound" </> "SC3" </> "UGen"

-- | Record.Plain directory
recp_dir :: FilePath
recp_dir = sc3_ugen_dir </> "Record" </> "Plain"

-- | Write module for a unit generator.
write_module :: DB.U -> IO ()
write_module u =
    let u' = Rename.u_rename u
        n = DB.ugen_name u
        m = [ printf "-- | %s" (DB.ugen_summary u)
            , printf "module Sound.SC3.UGen.Record.Plain.%s where" n
            , "import qualified Sound.SC3.Common as SC3"
            , "import qualified Sound.SC3.UGen as SC3"
            , "import qualified Sound.SC3.UGen.Bindings.HW.Construct as SC3"
            , "import Sound.SC3.UGen.Record.Plain" ]
        p = gen_param (u,u')
        d = gen_defaults (u,u')
        c = gen_cons u
        i = gen_make u
        fn = recp_dir </> n <.> ".hs"
    in writeFile fn (unlines (m ++ p ++ d ++ c ++ i))

-- > map DB.ugen_name ugen_db
ugen_db :: [DB.U]
ugen_db =
    let db = DB.ugen_db
        db' = sortBy (compare `on` DB.ugen_name) db
    in filter (\u -> DB.ugen_name u `elem` buildList) db'

main :: IO ()
main = do
  let us = filter (not . null . DB.ugen_inputs) ugen_db
  mapM_ write_module us
