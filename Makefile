all:
	(cd mk ; ghc -Wall build-plain.hs -e main)

mk-cmd:
	echo "hsc3-rec - NIL"

clean:
	rm -Rf dist dist-newstyle *~

delete-plain-modules:
	rm -f Sound/SC3/UGen/Record/Plain/*.hs

plain-module-list:
	echo "                   -- auto-generated: module-list:" $$(date) >> hsc3-rec.cabal
	find Sound/SC3/UGen/Record/Plain -name "*.*hs" | \
		sort | \
		sed	-e "s/^/                   /" \
			-e "s,/,\.,g" \
			-e "s,.hs,," >> hsc3-rec.cabal

push-all:
	r.gitlab-push.sh hsc3-rec
