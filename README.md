hsc3-rec
--------

[Haskell](http://haskell.org/)
[SuperCollider](http://audiosynth.com/)
record variants
(see [hsc3](?t=hsc3)).

- CRU
  + [Help/cru.help.hs](?t=hsc3-rec&e=Help/cru.help.hs)
  + [Help/Graph/jmcc-analog-bubbles.hs](?t=hsc3-rec&e=Help/Graph/cru/jmcc-analog-bubbles.hs)
- Plain
  + [Help/plain.help.hs](?t=hsc3-rec&e=Help/plain.help.hs)

© [rohan drape](http://rohandrape.net/) and others, 2008-2022, [gpl](http://gnu.org/copyleft/)
