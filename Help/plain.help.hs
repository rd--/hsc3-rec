-- default record with minimal field edits ; method ugen constructor
let n = lorenzTrigR {minfreq = 1, maxfreq = 8}
in sinOsc ar (decay (ugen n) 1.0 * 800 + 900) 0 * 0.4

-- plain record constructor ; plain ugen constructor
let n = LorenzTrig {rate = ar, minfreq = 1, maxfreq = 8, s = 10, r = 28, b = 2.66667, h = 0.02, x0 = 0.09088, y0 = 2.97077, z0 = 24.28204}
in sinOsc ar (decay (mkLorenzTrig n) 1.0 * 800 + 900) 0 * 0.4

---- ; notes
---- ; the hsc3 emacs mode is used, however additional modules must be loaded
import Sound.Sc3.Ugen.Record.Plain {- hsc3-rec -}
import Sound.Sc3.Ugen.Record.Plain.LorenzTrig {- hsc3-rec -}
