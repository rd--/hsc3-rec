-- pitch
let x = MouseX kr 220 660 Linear 0.1
    y = MouseY kr 0.05 0.25 Linear 0.1
    s = Saw ar x * y
    a = Amplitude kr s 0.05 0.05
    [f,_] = mceChannels (pitch {input = s})
in Out 0 (mce [s * 0.25,SinOsc ar (f / 2) 0 * a])

---- ; cru record modules
import Sound.Sc3.Ugen.Record.Cru {- hsc3-rec -}
import Sound.Sc3.Ugen.Record.Cru.Default {- hsc3-rec -}
