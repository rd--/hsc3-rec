let x = mouseX kr 220 660 Linear 0.1
    y = mouseY kr 0.05 0.25 Linear 0.1
    s = saw ar x * y
    a = amplitude kr s 0.05 0.05
    [f,_] = mceChannels (mkPitch (pitchR {input = s}))
in mce [s * 0.25,sinOsc ar (f / 2) 0 * a]

---- ; record module
import Sound.Sc3.Ugen.Record.Plain.Pitch {- hsc3-rec -}
