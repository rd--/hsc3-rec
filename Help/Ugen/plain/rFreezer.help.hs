-- rFreezer ; k-rate
let b = control KR "buf" 0
    n z i j = linLin (lfNoise2 z KR 0.1) (-1) 1 i j
in mkRFreezer
     RFreezer
     {rate = AR
     ,bufnum = b
     ,left = n 'α' 0.3 0.4
     ,right = n 'β' 0.5 0.6
     ,gain = n 'γ' 0.3 0.6
     ,increment = n 'δ' 0.95 1.05
     ,incrementOffset = n 'ε' 0.05 0.15
     ,incrementRandom = n 'ζ' 0.05 0.15
     ,rightRandom = n 'η' 0.05 0.15
     ,syncPhaseTrigger = 0
     ,randomizePhaseTrigger = 0
     ,numberOfLoops = 36}

---- ; record module
import Sound.Sc3.Ugen.Record.Plain.RFreezer
