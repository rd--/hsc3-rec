-- RShufflerB ; static
mkRShufflerB RShufflerB
  {rate = AR
  ,bufnum = control KR "buf" 0
  ,readLocationMinima = 0.0
  ,readLocationMaxima = 0.05
  ,readIncrementMinima = 0.95
  ,readIncrementMaxima = 1.05
  ,durationMinima = 0.035
  ,durationMaxima = 0.050
  ,envelopeAmplitudeMinima = 0.1
  ,envelopeAmplitudeMaxima = 0.2
  ,envelopeShapeMinima = 0.5
  ,envelopeShapeMaxima = 0.6
  ,envelopeSkewMinima = 0.4
  ,envelopeSkewMaxima = 0.6
  ,stereoLocationMinima = 0
  ,stereoLocationMaxima = 1
  ,interOffsetTimeMinima = 0.005
  ,interOffsetTimeMaxima = 0.010
  ,ftableReadLocationIncrement = 1
  ,readIncrementQuanta = 0
  ,interOffsetTimeQuanta = 0}

-- RShufflerB ; circulating record to buffer & static (record, use localBuf)
let r = RShufflerB
        {rate = AR
        ,bufnum = clearBuf (localBuf 'α' 1 (48000 * 4))
        ,readLocationMinima = 0.0
        ,readLocationMaxima = 0.05
        ,readIncrementMinima = 1.99975
        ,readIncrementMaxima = 2.00025
        ,durationMinima = 0.25
        ,durationMaxima = 0.30
        ,envelopeAmplitudeMinima = 0.8
        ,envelopeAmplitudeMaxima = 0.9
        ,envelopeShapeMinima = 0.5
        ,envelopeShapeMaxima = 0.6
        ,envelopeSkewMinima = 0.4
        ,envelopeSkewMaxima = 0.6
        ,stereoLocationMinima = 0
        ,stereoLocationMaxima = 1
        ,interOffsetTimeMinima = 0.0500
        ,interOffsetTimeMaxima = 0.0525
        ,ftableReadLocationIncrement = 1
        ,readIncrementQuanta = 0
        ,interOffsetTimeQuanta = 0}
    i = recordBuf AR (bufnum r) (2048 * 12) 1 0 1 Loop 1 DoNothing (soundIn 0)
in mrg2 (mkRShufflerB r) i

---- ; record module
import Sound.SC3.UGen.Record.Plain.RShufflerB {- hsc3-rec -}

---- ; control inputs
import Sound.SC3.UGen.DB.Record
putStrLn $ u_control_inputs_pp Sound.SC3.UGen.External.RDU.rShufflerB_dsc
