-- analog bubbles (jmcc) #1 ; default values
let o = lfSaw {rate = kr, freq = mce [8, 7.23]} * 3 + 80
    f = lfSaw {rate = kr, freq = 0.4} * 24 + o
    s = sinOsc {freq = midiCps f} * 0.04
    c = combN {input = s, decaytime = 4}
in out {input = c}

-- analog bubbles (jmcc) #1 ; no default values
let o = LFSaw {rate = kr, freq = mce [8, 7.23], iphase = 0} * 3 + 80
    f = LFSaw {rate = kr, freq = 0.4, iphase = 0} * 24 + o
    s = SinOsc {rate = ar, freq = midiCps f, phase = 0} * 0.04
    c = CombN {input = s, maxdelaytime = 0.2, delaytime = 0.2, decaytime = 4}
in Out {bus = 0, input = c}

-- analog bubbles (jmcc) #1 ; no field names
let o = LFSaw kr (UgenMce [8, 7.23]) 0 * 3 + 80
    f = LFSaw kr 0.4 0 * 24 + o
    s = SinOsc ar (midiCps f) 0 * 0.04
in CombN s 0.2 0.2 4
