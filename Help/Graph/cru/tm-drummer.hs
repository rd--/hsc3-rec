-- drummer (tm) ; no parameter names
let n = WhiteNoise ar
    tempo = 4
    tr = Impulse ar tempo 0
    tr_2 = PulseDivider tr 4 2
    tr_4 = PulseDivider tr 4 0
    snare = n * Decay2 tr_2 0.005 0.5
    bass = SinOsc ar 60 0 * Decay2 tr_4 0.005 0.5
    hihat = HPF n 10000 * Decay2 tr 0.005 0.5
in Pan2 (snare + bass + hihat) 0 0.4

-- drummer (tm) ; no defaults
let n = WhiteNoise {rate = ar}
    tempo = 4
    tr = Impulse {rate = ar, freq = tempo, phase = 0}
    tr_2 = PulseDivider {trig_ = tr, div_ = 4, start = 2}
    tr_4 = PulseDivider {trig_ = tr, div_ = 4, start = 0}
    snare = n * Decay2 {input = tr_2, attackTime = 0.005, decayTime = 0.5}
    bass = SinOsc {rate = ar, freq = 60, phase = 0} * Decay2 {input = tr_4, attackTime = 0.005, decayTime = 0.5}
    hihat = HPF {input = n, freq = 10000} * Decay2 {input = tr, attackTime = 0.005, decayTime = 0.5}
in Pan2 {input = snare + bass + hihat, pos = 0, level = 0.4}
