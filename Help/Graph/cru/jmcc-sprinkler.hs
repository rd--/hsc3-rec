-- sprinkler (jmcc) #1 ; defaults
let f = lfPulse {rate = kr, freq = 0.09, width = 0.16} * 10 + 7
    t = lfPulse {rate = kr, freq = f, width = 0.25} * 0.1
in out {input = bpz2 {input = whiteNoise * t}}

-- sprinkler (jmcc) #1 ; no defaults
let f = LFPulse {rate = kr, freq = 0.09, iphase = 0, width = 0.16} * 10 + 7
    t = LFPulse {rate = kr, freq = f, iphase = 0, width = 0.25} * 0.1
in Out {bus = 0, input = BPZ2 {input = WhiteNoise {rate = ar} * t}}
