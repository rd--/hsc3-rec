{-# LANGUAGE DisambiguateRecordFields #-}

import Sound.Sc3 {- hsc3 -}

import Sound.Sc3.Ugen.Record.Plain {- hsc3-rec -}
import Sound.Sc3.Ugen.Record.Plain.CombN {- hsc3-rec -}
import Sound.Sc3.Ugen.Record.Plain.LFSaw {- hsc3-rec -}
import Sound.Sc3.Ugen.Record.Plain.Out {- hsc3-rec -}
import Sound.Sc3.Ugen.Record.Plain.SinOsc {- hsc3-rec -}

analog_bubbles :: Ugen
analog_bubbles =
    let o = ugen
          LFSaw
          {rate = kr
          ,freq = mce2 8 7.23
          ,iphase = 0} * 3 + 80
        f = ugen
          LFSaw
          {rate = kr
          ,freq = 0.4
          ,iphase = 0} * 24 + o
        s = ugen
          SinOsc
          {rate = ar
          ,freq = midiCps f
          ,phase = 0} * 0.04
        c = ugen
          CombN
          {rate = ar
          ,input = s
          ,maxdelaytime = 0.2
          ,delaytime = 0.2
          ,decaytime = 4}
    in ugen
    Out
    {rate = ar
    ,bus = mce2 0 1
    ,input = c}

main :: IO ()
main = audition analog_bubbles

{-
Sound.Sc3.Ugen.Dot.draw analog_bubbles
-}
