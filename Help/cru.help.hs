-- record constructors ; labeled arguments
let p = SinOsc {rate = ar, freq = 440, phase = 0}
    q = Saw {rate = kr, freq = 3}
    r = Saw {rate = ar, freq = q * 30 + 90}
in Out {bus = 0, input = mce [p, r] * 0.1}

-- default records ; field rewriting
let p = sinOsc
    q = sinOsc {freq = 441 + saw {rate = kr, freq = 1}}
in out {input = mce [p, q] * 0.1}

-- field update
let o = sinOsc {freq = lfSaw {freq = 0.4} * 24 + 200}
in out {input = o + o {freq = freq o * lfSaw {freq = 0.1}}}

---- ; notes
---- ; the hsc3 emacs mode is used, however the Cru module alone must be loaded
---- ; (c.f. hsc3-unload-all-modules)
import Sound.Sc3.Ugen.Record.Cru {- hsc3-rec -}
import Sound.Sc3.Ugen.Record.Cru.Default {- hsc3-rec -}
import Sound.Sc3.Common.Math.Operator {- hsc3 -}

---- ; using fields that don't exist is a run-time (not compile-time) error
sinOsc {width = 0.5}
