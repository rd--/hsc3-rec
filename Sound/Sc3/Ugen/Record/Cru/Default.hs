module Sound.Sc3.Ugen.Record.Cru.Default where

import Sound.Sc3.Common.Enum {- hsc3 -}
import Sound.Sc3.Common.Envelope {- hsc3 -}
import Sound.Sc3.Common.Rate {- hsc3 -}

import Sound.Sc3.Ugen.Record.Cru.Type

-- AUTOGEN/EDIT
envGen :: Ugen
envGen = EnvGen ar 1.0 1.0 0.0 1.0 DoNothing (envPerc 0.1 1.0)
mouseX :: Ugen
mouseX = MouseX kr 0.0 1.0 Linear 0.2
mouseY :: Ugen
mouseY = MouseY kr 0.0 1.0 Linear 0.2
xLine :: Ugen
xLine = XLine ar 1.0 2.0 1.0 DoNothing

-- AUTOGEN
allpassN :: Ugen
allpassN = AllpassN 0.0 0.2 0.2 1.0
amplitude :: Ugen
amplitude = Amplitude ar 0.0 1.0e-2 1.0e-2
bpz2 :: Ugen
bpz2 = BPZ2 0.0
combL :: Ugen
combL = CombL 0.0 0.2 0.2 1.0
combN :: Ugen
combN = CombN 0.0 0.2 0.2 1.0
decay :: Ugen
decay = Decay 0.0 1.0
decay2 :: Ugen
decay2 = Decay2 0.0 1.0e-2 1.0
demand :: Ugen
demand = Demand 0.0 0.0 0.0
dseq :: Ugen
dseq = Dseq 1.0 0.0
dshuf :: Ugen
dshuf = Dshuf 1.0 0.0
dust :: Ugen
dust = Dust ar 0.0
expRand :: Ugen
expRand = ExpRand 1.0e-2 1.0
fSinOsc :: Ugen
fSinOsc = FSinOsc ar 440.0 0.0
hpf :: Ugen
hpf = HPF 0.0 440.0
hpz1 :: Ugen
hpz1 = HPZ1 0.0
impulse :: Ugen
impulse = Impulse ar 440.0 0.0
iRand :: Ugen
iRand = IRand 0.0 1.0
klang :: Ugen
klang = Klang ar 1.0 0.0 0.0
klank :: Ugen
klank = Klank 0.0 1.0 0.0 1.0 0.0
lfNoise0 :: Ugen
lfNoise0 = LFNoise0 ar 500.0
lfNoise1 :: Ugen
lfNoise1 = LFNoise1 ar 500.0
lfNoise2 :: Ugen
lfNoise2 = LFNoise2 ar 500.0
lfPulse :: Ugen
lfPulse = LFPulse ar 440.0 0.0 0.5
lfSaw :: Ugen
lfSaw = LFSaw ar 440.0 0.0
lpf :: Ugen
lpf = LPF 0.0 440.0
lag :: Ugen
lag = Lag 0.0 0.1
latch :: Ugen
latch = Latch 0.0 0.0
leakDC :: Ugen
leakDC = LeakDC 0.0 0.995
out :: Ugen
out = Out 0.0 0.0
pan2 :: Ugen
pan2 = Pan2 0.0 0.0 1.0
pinkNoise :: Ugen
pinkNoise = PinkNoise ar
pitch :: Ugen
pitch = Pitch 0.0 440.0 60.0 4000.0 100.0 16.0 1.0 1.0e-2 0.5 1.0 0.0
pulse :: Ugen
pulse = Pulse ar 440.0 0.5
pulseDivider :: Ugen
pulseDivider = PulseDivider 0.0 2.0 0.0
rlpf :: Ugen
rlpf = RLPF 0.0 440.0 1.0
rand :: Ugen
rand = Rand 0.0 1.0
randN :: Ugen
randN = RandN 1 1.0e-4 1.0
resonz :: Ugen
resonz = Resonz 0.0 440.0 1.0
saw :: Ugen
saw = Saw ar 440.0
sinOsc :: Ugen
sinOsc = SinOsc ar 440.0 0.0
whiteNoise :: Ugen
whiteNoise = WhiteNoise ar
