module Sound.Sc3.Ugen.Record.Cru.Type where

import Sound.Sc3.Common.Enum {- hsc3 -}
import Sound.Sc3.Common.Envelope hiding (Linen) {- hsc3 -}
import qualified Sound.Sc3.Common.Math.Operator as Sc3 {- hsc3 -}
import Sound.Sc3.Common.Rate {- hsc3 -}

import qualified Sound.Sc3.Server.Transport.Monad as Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Bindings as Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Bindings.Db.External as External {- hsc3 -}
import qualified Sound.Sc3.Ugen.Ugen as Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Util as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Ugen.Dot as Dot {- hsc3-dot -}

-- * Lifting & Instances

-- | Lift unary 'Sc3.Ugen' transform.
ugenLift :: (Sc3.Ugen -> Sc3.Ugen) -> Ugen -> Ugen
ugenLift f p = UgenPrimitive (f (ugen p))

-- | Lift binary 'Sc3.Ugen' transform.
ugenLift2 :: (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen) -> Ugen -> Ugen -> Ugen
ugenLift2 f p q = UgenPrimitive (f (ugen p) (ugen q))

instance Num Ugen where
    (+) = ugenLift2 (+)
    (*) = ugenLift2 (*)
    (-) = ugenLift2 (-)
    negate = ugenLift negate
    abs = ugenLift abs
    signum = ugenLift signum
    fromInteger = UgenConstant . fromInteger

instance Fractional Ugen where
    (/) = ugenLift2 (/)
    fromRational = UgenConstant . fromRational

instance Eq Ugen where
    (==) = error "Ugen.Eq, see EqE"

instance Ord Ugen where
    (<) = error "Ugen.<, see <**"
    (<=) = error "Ugen.<= at, see <=**"
    (>) = error "Ugen.>, see >**"
    (>=) = error "Ugen.>=, see >=**"
    min = ugenLift2 min
    max = ugenLift2 max

instance Sc3.OrdE Ugen where
    greater_than = ugenLift2 Sc3.greater_than
    greater_than_or_equal_to = ugenLift2 Sc3.greater_than_or_equal_to
    less_than = ugenLift2 Sc3.less_than
    less_than_or_equal_to = ugenLift2 Sc3.less_than_or_equal_to

instance Floating Ugen where
    pi = UgenConstant pi
    exp = error "Ugen/Floating is partial"
    log = error "Ugen/Floating is partial"
    sqrt = error "Ugen/Floating is partial"
    (**) = error "Ugen/Floating is partial"
    logBase = error "Ugen/Floating is partial"
    sin = error "Ugen/Floating is partial"
    cos = error "Ugen/Floating is partial"
    tan = error "Ugen/Floating is partial"
    asin = error "Ugen/Floating is partial"
    acos = error "Ugen/Floating is partial"
    atan = error "Ugen/Floating is partial"
    sinh = error "Ugen/Floating is partial"
    cosh = error "Ugen/Floating is partial"
    tanh = error "Ugen/Floating is partial"
    asinh = error "Ugen/Floating is partial"
    acosh = error "Ugen/Floating is partial"
    atanh = error "Ugen/Floating is partial"

-- * MCE

mce :: [Ugen] -> Ugen
mce = UgenPrimitive . Sc3.mce . map ugen

mceChannels :: Ugen -> [Ugen]
mceChannels = map UgenPrimitive . Sc3.mceChannels . ugen

mceReverse :: Ugen -> Ugen
mceReverse = UgenPrimitive . Sc3.mceReverse . ugen

mce2 :: Ugen -> Ugen -> Ugen
mce2 x y = mce [x,y]

-- * Ugen input re-writers

klankSpec_mce :: Ugen -> Ugen -> Ugen -> Ugen
klankSpec_mce = Sc3.klanx_spec_f mceChannels mce

-- * Constants

dinf :: Ugen
dinf = UgenConstant 9e8

-- * IO (draw and audition)

wrapOut :: Maybe Double -> Ugen -> Ugen
wrapOut t u = UgenPrimitive (Sc3.wrapOut t (ugen u))

instance Dot.Drawable Ugen where
    dot_with_opt o u = Dot.dot_with_opt o (ugen u)

instance Sc3.Audible Ugen where
    playAt o = Sc3.playAt o . ugen

-- * Unary Operators

instance Sc3.UnaryOp Ugen where
  midiCps = UgenPrimitive . Sc3.midiCps . ugen
  cpsMidi = UgenPrimitive . Sc3.cpsMidi . ugen

-- * Ugen Definitions

data Ugen = UgenConstant Double
           | UgenMce [Ugen]
           | UgenPrimitive Sc3.Ugen
           -- AUTOGEN
           | AllpassN { input :: Ugen , maxdelaytime :: Ugen , delaytime :: Ugen , decaytime :: Ugen }
           | Amplitude { rate :: Rate , input :: Ugen , attackTime :: Ugen , releaseTime :: Ugen }
           | BPZ2 { input :: Ugen }
           | CombL { input :: Ugen , maxdelaytime :: Ugen , delaytime :: Ugen , decaytime :: Ugen }
           | CombN { input :: Ugen , maxdelaytime :: Ugen , delaytime :: Ugen , decaytime :: Ugen }
           | Decay { input :: Ugen , decayTime :: Ugen }
           | Decay2 { input :: Ugen , attackTime :: Ugen , decayTime :: Ugen }
           | Demand { trig_ :: Ugen , reset :: Ugen , demandUgens :: Ugen }
           | Dseq { repeats :: Ugen , list_ :: Ugen }
           | Dshuf { repeats :: Ugen , list_ :: Ugen }
           | Dust { rate :: Rate , density :: Ugen }
           | EnvGen { rate :: Rate , gate_ :: Ugen , levelScale :: Ugen , levelBias :: Ugen , timeScale :: Ugen , doneAction :: DoneAction Ugen , envelope_ :: Envelope Ugen }
           | ExpRand { lo :: Ugen , hi :: Ugen }
           | FSinOsc { rate :: Rate , freq :: Ugen , iphase :: Ugen }
           | HPF { input :: Ugen , freq :: Ugen }
           | HPZ1 { input :: Ugen }
           | IRand { lo :: Ugen , hi :: Ugen }
           | Impulse { rate :: Rate , freq :: Ugen , phase :: Ugen }
           | Klang { rate :: Rate , freqscale :: Ugen , freqoffset :: Ugen , specificationsArrayRef :: Ugen }
           | Klank { input :: Ugen , freqscale :: Ugen , freqoffset :: Ugen , decayscale :: Ugen , specificationsArrayRef :: Ugen }
           | LFNoise0 { rate :: Rate , freq :: Ugen }
           | LFNoise1 { rate :: Rate , freq :: Ugen }
           | LFNoise2 { rate :: Rate , freq :: Ugen }
           | LorenzTrig { rate :: Rate , minfreq :: Ugen , maxfreq :: Ugen , s :: Ugen , r :: Ugen , b :: Ugen , h :: Ugen , x0 :: Ugen , y0 :: Ugen , z0 :: Ugen }
           | LFPulse { rate :: Rate , freq :: Ugen , iphase :: Ugen , width :: Ugen }
           | LFSaw { rate :: Rate , freq :: Ugen , iphase :: Ugen }
           | LPF { input :: Ugen , freq :: Ugen }
           | Lag { input :: Ugen , lagTime :: Ugen }
           | Latch { input :: Ugen , trig_ :: Ugen }
           | LeakDC { input :: Ugen , coef :: Ugen }
           | Line { rate :: Rate , start :: Ugen , end :: Ugen , dur :: Ugen , doneAction :: DoneAction Ugen }
           | Linen { gate_ :: Ugen , attackTime :: Ugen , susLevel :: Ugen , releaseTime :: Ugen , doneAction :: DoneAction Ugen }
           | MouseX { rate :: Rate , minval :: Ugen , maxval :: Ugen , warp :: Warp Ugen , lag_ :: Ugen }
           | MouseY { rate :: Rate , minval :: Ugen , maxval :: Ugen , warp :: Warp Ugen , lag_ :: Ugen }
           | Out { bus :: Ugen , input :: Ugen }
           | Pan2 { input :: Ugen , pos :: Ugen , level :: Ugen }
           | PinkNoise { rate :: Rate }
           | Pitch { input :: Ugen , initFreq :: Ugen , minFreq :: Ugen , maxFreq :: Ugen , execFreq :: Ugen , maxBinsPerOctave :: Ugen , median_ :: Ugen , ampThreshold :: Ugen , peakThreshold :: Ugen , downSample :: Ugen , clar :: Ugen }
           | Pulse { rate :: Rate , freq :: Ugen , width :: Ugen }
           | PulseDivider { trig_ :: Ugen , div_ :: Ugen , start :: Ugen }
           | RLPF { input :: Ugen , freq :: Ugen , rq :: Ugen }
           | Rand { lo :: Ugen , hi :: Ugen }
           | RandN { nc :: Int , lo :: Ugen , hi :: Ugen }
           | Resonz { input :: Ugen , freq :: Ugen , bwr :: Ugen }
           | Saw { rate :: Rate , freq :: Ugen }
           | SinOsc { rate :: Rate , freq :: Ugen , phase :: Ugen }
           | WhiteNoise { rate :: Rate }
           | XLine { rate :: Rate , start :: Ugen , end :: Ugen , dur :: Ugen , doneAction :: DoneAction Ugen }
             deriving (Show)

ugen :: Ugen -> Sc3.Ugen
ugen u =
    case u of
      UgenConstant c -> Sc3.constant c
      UgenMce l -> Sc3.mce (map ugen l)
      UgenPrimitive u' -> u'
      -- AUTOGEN
      AllpassN _in _maxdelaytime _delaytime _decaytime -> Sc3.allpassN (ugen _in) (ugen _maxdelaytime) (ugen _delaytime) (ugen _decaytime)
      Amplitude _rate _in _attackTime _releaseTime -> Sc3.amplitude _rate (ugen _in) (ugen _attackTime) (ugen _releaseTime)
      BPZ2 _in -> Sc3.bpz2 (ugen _in)
      CombL _in _maxdelaytime _delaytime _decaytime -> Sc3.combL (ugen _in) (ugen _maxdelaytime) (ugen _delaytime) (ugen _decaytime)
      CombN _in _maxdelaytime _delaytime _decaytime -> Sc3.combN (ugen _in) (ugen _maxdelaytime) (ugen _delaytime) (ugen _decaytime)
      Decay _in _decayTime -> Sc3.decay (ugen _in) (ugen _decayTime)
      Decay2 _in _attackTime _decayTime -> Sc3.decay2 (ugen _in) (ugen _attackTime) (ugen _decayTime)
      Demand _trig _reset _demandUgens -> Sc3.demand (ugen _trig) (ugen _reset) (ugen _demandUgens)
      Dseq _repeats _list -> Sc3.dseq (ugen _repeats) (ugen _list)
      Dshuf _repeats _list -> Sc3.dshuf (ugen _repeats) (ugen _list)
      Dust _rate _density -> Sc3.dust _rate (ugen _density)
      EnvGen _rate _gate _levelScale _levelBias _timeScale _doneAction _envelope -> Sc3.envGen _rate (ugen _gate) (ugen _levelScale) (ugen _levelBias) (ugen _timeScale) (fmap ugen _doneAction) (envelope_map ugen _envelope)
      ExpRand _lo _hi -> Sc3.expRand (ugen _lo) (ugen _hi)
      FSinOsc _rate _freq _iphase -> Sc3.fSinOsc _rate (ugen _freq) (ugen _iphase)
      HPF _in _freq -> Sc3.hpf (ugen _in) (ugen _freq)
      HPZ1 _in -> Sc3.hpz1 (ugen _in)
      Impulse _rate _freq _phase -> Sc3.impulse _rate (ugen _freq) (ugen _phase)
      IRand _lo _hi -> Sc3.iRand (ugen _lo) (ugen _hi)
      Klang _rate _freqscale _freqoffset _specificationsArrayRef -> Sc3.klang _rate (ugen _freqscale) (ugen _freqoffset) (ugen _specificationsArrayRef)
      Klank _input _freqscale _freqoffset _decayscale _specificationsArrayRef -> Sc3.klank (ugen _input) (ugen _freqscale) (ugen _freqoffset) (ugen _decayscale) (ugen _specificationsArrayRef)
      LFNoise0 _rate _freq -> Sc3.lfNoise0 _rate (ugen _freq)
      LFNoise1 _rate _freq -> Sc3.lfNoise1 _rate (ugen _freq)
      LFNoise2 _rate _freq -> Sc3.lfNoise2 _rate (ugen _freq)
      LorenzTrig _rate _minfreq _maxfreq _s _r _b _h _x0 _y0 _z0 -> External.lorenzTrig _rate (ugen _minfreq) (ugen _maxfreq) (ugen _s) (ugen _r) (ugen _b) (ugen _h) (ugen _x0) (ugen _y0) (ugen _z0)
      LFPulse _rate _freq _iphase _width -> Sc3.lfPulse _rate (ugen _freq) (ugen _iphase) (ugen _width)
      LFSaw _rate _freq _iphase -> Sc3.lfSaw _rate (ugen _freq) (ugen _iphase)
      LPF _in _freq -> Sc3.lpf (ugen _in) (ugen _freq)
      Lag _in _lagTime -> Sc3.lag (ugen _in) (ugen _lagTime)
      Latch _in _trig -> Sc3.latch (ugen _in) (ugen _trig)
      LeakDC _in _coef -> Sc3.leakDC (ugen _in) (ugen _coef)
      Line _rate _start _end _dur _doneAction -> Sc3.line _rate (ugen _start) (ugen _end) (ugen _dur) (fmap ugen _doneAction)
      Linen _gate _attackTime _susLevel _releaseTime _doneAction -> Sc3.linen (ugen _gate) (ugen _attackTime) (ugen _susLevel) (ugen _releaseTime) (fmap ugen _doneAction)
      MouseX _rate _minval _maxval _warp _lag -> Sc3.mouseX _rate (ugen _minval) (ugen _maxval) (fmap ugen _warp) (ugen _lag)
      MouseY _rate _minval _maxval _warp _lag -> Sc3.mouseY _rate (ugen _minval) (ugen _maxval) (fmap ugen _warp) (ugen _lag)
      Out _bus _channelsArray -> Sc3.out (ugen _bus) (ugen _channelsArray)
      Pan2 _in _pos _level -> Sc3.pan2 (ugen _in) (ugen _pos) (ugen _level)
      PinkNoise _rate -> Sc3.pinkNoise _rate
      Pitch _in _initFreq _minFreq _maxFreq _execFreq _maxBinsPerOctave _median _ampThreshold _peakThreshold _downSample _clar -> Sc3.pitch (ugen _in) (ugen _initFreq) (ugen _minFreq) (ugen _maxFreq) (ugen _execFreq) (ugen _maxBinsPerOctave) (ugen _median) (ugen _ampThreshold) (ugen _peakThreshold) (ugen _downSample) (ugen _clar)
      Pulse _rate _freq _width -> Sc3.pulse _rate (ugen _freq) (ugen _width)
      PulseDivider _trig _div _start -> Sc3.pulseDivider (ugen _trig) (ugen _div) (ugen _start)
      RLPF _in _freq _rq -> Sc3.rlpf (ugen _in) (ugen _freq) (ugen _rq)
      Rand _lo _hi -> Sc3.rand (ugen _lo) (ugen _hi)
      RandN _nc _lo _hi -> External.randN _nc (ugen _lo) (ugen _hi)
      Resonz _in _freq _bwr -> Sc3.resonz (ugen _in) (ugen _freq) (ugen _bwr)
      Saw _rate _freq -> Sc3.saw _rate (ugen _freq)
      SinOsc _rate _freq _phase -> Sc3.sinOsc _rate (ugen _freq) (ugen _phase)
      WhiteNoise _rate -> Sc3.whiteNoise _rate
      XLine _rate _start _end _dur _doneAction -> Sc3.xLine _rate (ugen _start) (ugen _end) (ugen _dur) (fmap ugen _doneAction)

-- Local Variables:
-- truncate-lines:t
-- End:
