-- | Demand rate sequence generator.
module Sound.Sc3.Ugen.Record.Plain.Dseq where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Dseq = Dseq {
  rate :: Sc3.Rate,
  repeats :: Sc3.Ugen,
  list_ :: Sc3.Ugen
  } deriving (Show)
dseqR :: Dseq
dseqR = Dseq {
  rate = Sc3.dr,
  repeats = 1.0,
  list_ = 0.0
  }
mkDseq :: Dseq -> Sc3.Ugen
mkDseq (Dseq r a' b') = Sc3.mkOsc r "Dseq" [a',b'] 1
instance Make Dseq where
  ugen = mkDseq
