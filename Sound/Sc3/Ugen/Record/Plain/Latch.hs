-- | Sample and hold
module Sound.Sc3.Ugen.Record.Plain.Latch where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Latch = Latch {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  trig_ :: Sc3.Ugen
  } deriving (Show)
latchR :: Latch
latchR = Latch {
  rate = Sc3.ar,
  input = 0.0,
  trig_ = 0.0
  }
mkLatch :: Latch -> Sc3.Ugen
mkLatch (Latch r a' b') = Sc3.mkOsc r "Latch" [a',b'] 1
instance Make Latch where
  ugen = mkLatch
