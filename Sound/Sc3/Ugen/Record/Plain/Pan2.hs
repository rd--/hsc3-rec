-- | Two channel equal power pan.
module Sound.Sc3.Ugen.Record.Plain.Pan2 where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Pan2 = Pan2 {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  pos :: Sc3.Ugen,
  level :: Sc3.Ugen
  } deriving (Show)
pan2R :: Pan2
pan2R = Pan2 {
  rate = Sc3.ar,
  input = 0.0,
  pos = 0.0,
  level = 1.0
  }
mkPan2 :: Pan2 -> Sc3.Ugen
mkPan2 (Pan2 r a' b' c') = Sc3.mkOsc r "Pan2" [a',b',c'] 2
instance Make Pan2 where
  ugen = mkPan2
