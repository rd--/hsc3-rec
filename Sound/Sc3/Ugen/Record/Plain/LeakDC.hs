-- | Remove DC
module Sound.Sc3.Ugen.Record.Plain.LeakDC where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data LeakDC = LeakDC {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  coef :: Sc3.Ugen
  } deriving (Show)
leakDCR :: LeakDC
leakDCR = LeakDC {
  rate = Sc3.ar,
  input = 0.0,
  coef = 0.995
  }
mkLeakDC :: LeakDC -> Sc3.Ugen
mkLeakDC (LeakDC r a' b') = Sc3.mkOsc r "LeakDC" [a',b'] 1
instance Make LeakDC where
  ugen = mkLeakDC
