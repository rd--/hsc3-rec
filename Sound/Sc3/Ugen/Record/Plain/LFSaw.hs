-- | Sawtooth oscillator
module Sound.Sc3.Ugen.Record.Plain.LFSaw where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data LFSaw = LFSaw {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen,
  iphase :: Sc3.Ugen
  } deriving (Show)
lfSawR :: LFSaw
lfSawR = LFSaw {
  rate = Sc3.ar,
  freq = 440.0,
  iphase = 0.0
  }
mkLFSaw :: LFSaw -> Sc3.Ugen
mkLFSaw (LFSaw r a' b') = Sc3.mkOsc r "LFSaw" [a',b'] 1
instance Make LFSaw where
  ugen = mkLFSaw
