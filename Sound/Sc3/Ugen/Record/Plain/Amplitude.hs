-- | Amplitude follower
module Sound.Sc3.Ugen.Record.Plain.Amplitude where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Amplitude = Amplitude {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  attackTime :: Sc3.Ugen,
  releaseTime :: Sc3.Ugen
  } deriving (Show)
amplitudeR :: Amplitude
amplitudeR = Amplitude {
  rate = Sc3.ar,
  input = 0.0,
  attackTime = 0.01,
  releaseTime = 0.01
  }
mkAmplitude :: Amplitude -> Sc3.Ugen
mkAmplitude (Amplitude r a' b' c') = Sc3.mkOsc r "Amplitude" [a',b',c'] 1
instance Make Amplitude where
  ugen = mkAmplitude
