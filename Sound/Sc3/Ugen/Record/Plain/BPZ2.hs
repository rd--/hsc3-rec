-- | Two zero fixed midpass.
module Sound.Sc3.Ugen.Record.Plain.BPZ2 where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data BPZ2 = BPZ2 {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen
  } deriving (Show)
bpz2R :: BPZ2
bpz2R = BPZ2 {
  rate = Sc3.ar,
  input = 0.0
  }
mkBPZ2 :: BPZ2 -> Sc3.Ugen
mkBPZ2 (BPZ2 r a') = Sc3.mkOsc r "BPZ2" [a'] 1
instance Make BPZ2 where
  ugen = mkBPZ2
