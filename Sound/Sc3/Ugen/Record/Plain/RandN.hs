-- | (Undocumented class)
module Sound.Sc3.Ugen.Record.Plain.RandN where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data RandN = RandN {
  rate :: Sc3.Rate,
  numChannels :: Sc3.Ugen,
  lo :: Sc3.Ugen,
  hi :: Sc3.Ugen
  } deriving (Show)
randNR :: RandN
randNR = RandN {
  rate = Sc3.ir,
  numChannels = 2.0,
  lo = 0.0,
  hi = 1.0
  }
mkRandN :: RandN -> Sc3.Ugen
mkRandN (RandN r a' b' c') = Sc3.mkOsc r "RandN" [a',b',c'] 2
instance Make RandN where
  ugen = mkRandN
