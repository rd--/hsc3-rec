-- | Two point difference filter
module Sound.Sc3.Ugen.Record.Plain.HPZ1 where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data HPZ1 = HPZ1 {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen
  } deriving (Show)
hpz1R :: HPZ1
hpz1R = HPZ1 {
  rate = Sc3.ar,
  input = 0.0
  }
mkHPZ1 :: HPZ1 -> Sc3.Ugen
mkHPZ1 (HPZ1 r a') = Sc3.mkOsc r "HPZ1" [a'] 1
instance Make HPZ1 where
  ugen = mkHPZ1
