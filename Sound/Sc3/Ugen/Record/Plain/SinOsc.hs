-- | Interpolating sine wavetable oscillator.
module Sound.Sc3.Ugen.Record.Plain.SinOsc where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data SinOsc = SinOsc {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen,
  phase :: Sc3.Ugen
  } deriving (Show)
sinOscR :: SinOsc
sinOscR = SinOsc {
  rate = Sc3.ar,
  freq = 440.0,
  phase = 0.0
  }
mkSinOsc :: SinOsc -> Sc3.Ugen
mkSinOsc (SinOsc r a' b') = Sc3.mkOsc r "SinOsc" [a',b'] 1
instance Make SinOsc where
  ugen = mkSinOsc
