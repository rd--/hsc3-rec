-- | Demand rate random sequence generator
module Sound.Sc3.Ugen.Record.Plain.Dshuf where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Dshuf = Dshuf {
  rate :: Sc3.Rate,
  repeats :: Sc3.Ugen,
  list_ :: Sc3.Ugen
  } deriving (Show)
dshufR :: Dshuf
dshufR = Dshuf {
  rate = Sc3.dr,
  repeats = 1.0,
  list_ = 0.0
  }
mkDshuf :: Dshuf -> Sc3.Ugen
mkDshuf (Dshuf r a' b') = Sc3.mkOsc r "Dshuf" [a',b'] 1
instance Make Dshuf where
  ugen = mkDshuf
