-- | Cursor tracking Ugen.
module Sound.Sc3.Ugen.Record.Plain.MouseY where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data MouseY = MouseY {
  rate :: Sc3.Rate,
  minval :: Sc3.Ugen,
  maxval :: Sc3.Ugen,
  warp :: Sc3.Ugen,
  lag :: Sc3.Ugen
  } deriving (Show)
mouseYR :: MouseY
mouseYR = MouseY {
  rate = Sc3.kr,
  minval = 0.0,
  maxval = 1.0,
  warp = 0.0,
  lag = 0.2
  }
mkMouseY :: MouseY -> Sc3.Ugen
mkMouseY (MouseY r a' b' c' d') = Sc3.mkOsc r "MouseY" [a',b',c',d'] 1
instance Make MouseY where
  ugen = mkMouseY
