-- | Resonant filter.
module Sound.Sc3.Ugen.Record.Plain.Resonz where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Resonz = Resonz {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  freq :: Sc3.Ugen,
  bwr :: Sc3.Ugen
  } deriving (Show)
resonzR :: Resonz
resonzR = Resonz {
  rate = Sc3.ar,
  input = 0.0,
  freq = 440.0,
  bwr = 1.0
  }
mkResonz :: Resonz -> Sc3.Ugen
mkResonz (Resonz r a' b' c') = Sc3.mkOsc r "Resonz" [a',b',c'] 1
instance Make Resonz where
  ugen = mkResonz
