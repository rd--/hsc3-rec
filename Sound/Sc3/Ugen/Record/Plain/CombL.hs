-- | Comb delay line with linear interpolation.
module Sound.Sc3.Ugen.Record.Plain.CombL where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data CombL = CombL {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  maxdelaytime :: Sc3.Ugen,
  delaytime :: Sc3.Ugen,
  decaytime :: Sc3.Ugen
  } deriving (Show)
combLR :: CombL
combLR = CombL {
  rate = Sc3.ar,
  input = 0.0,
  maxdelaytime = 0.2,
  delaytime = 0.2,
  decaytime = 1.0
  }
mkCombL :: CombL -> Sc3.Ugen
mkCombL (CombL r a' b' c' d') = Sc3.mkOsc r "CombL" [a',b',c',d'] 1
instance Make CombL where
  ugen = mkCombL
