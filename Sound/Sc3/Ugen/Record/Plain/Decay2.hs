-- | Exponential decay
module Sound.Sc3.Ugen.Record.Plain.Decay2 where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Decay2 = Decay2 {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  attackTime :: Sc3.Ugen,
  decayTime :: Sc3.Ugen
  } deriving (Show)
decay2R :: Decay2
decay2R = Decay2 {
  rate = Sc3.ar,
  input = 0.0,
  attackTime = 0.01,
  decayTime = 1.0
  }
mkDecay2 :: Decay2 -> Sc3.Ugen
mkDecay2 (Decay2 r a' b' c') = Sc3.mkOsc r "Decay2" [a',b',c'] 1
instance Make Decay2 where
  ugen = mkDecay2
