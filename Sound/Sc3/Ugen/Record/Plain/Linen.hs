-- | Simple linear envelope generator.
module Sound.Sc3.Ugen.Record.Plain.Linen where
import qualified Sound.Sc3.Common.Rate as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Linen = Linen {
  rate :: Sc3.Rate,
  gate_ :: Sc3.Ugen,
  attackTime :: Sc3.Ugen,
  susLevel :: Sc3.Ugen,
  releaseTime :: Sc3.Ugen,
  doneAction :: Sc3.Ugen
  } deriving (Show)
linenR :: Linen
linenR = Linen {
  rate = Sc3.kr,
  gate_ = 1.0,
  attackTime = 0.01,
  susLevel = 1.0,
  releaseTime = 1.0,
  doneAction = 0.0
  }
mkLinen :: Linen -> Sc3.Ugen
mkLinen (Linen r a' b' c' d' e') = Sc3.mkOsc r "Linen" [a',b',c',d',e'] 1
instance Make Linen where
  ugen = mkLinen
