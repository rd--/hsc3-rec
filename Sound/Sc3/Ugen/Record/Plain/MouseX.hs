-- | Cursor tracking Ugen.
module Sound.Sc3.Ugen.Record.Plain.MouseX where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data MouseX = MouseX {
  rate :: Sc3.Rate,
  minval :: Sc3.Ugen,
  maxval :: Sc3.Ugen,
  warp :: Sc3.Ugen,
  lag :: Sc3.Ugen
  } deriving (Show)
mouseXR :: MouseX
mouseXR = MouseX {
  rate = Sc3.kr,
  minval = 0.0,
  maxval = 1.0,
  warp = 0.0,
  lag = 0.2
  }
mkMouseX :: MouseX -> Sc3.Ugen
mkMouseX (MouseX r a' b' c' d') = Sc3.mkOsc r "MouseX" [a',b',c',d'] 1
instance Make MouseX where
  ugen = mkMouseX
