-- | Band limited pulse wave.
module Sound.Sc3.Ugen.Record.Plain.Pulse where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Pulse = Pulse {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen,
  width :: Sc3.Ugen
  } deriving (Show)
pulseR :: Pulse
pulseR = Pulse {
  rate = Sc3.ar,
  freq = 440.0,
  width = 0.5
  }
mkPulse :: Pulse -> Sc3.Ugen
mkPulse (Pulse r a' b') = Sc3.mkOsc r "Pulse" [a',b'] 1
instance Make Pulse where
  ugen = mkPulse
