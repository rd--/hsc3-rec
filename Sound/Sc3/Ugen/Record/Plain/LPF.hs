-- | 2nd order Butterworth lowpass filter
module Sound.Sc3.Ugen.Record.Plain.LPF where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data LPF = LPF {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  freq :: Sc3.Ugen
  } deriving (Show)
lpfR :: LPF
lpfR = LPF {
  rate = Sc3.ar,
  input = 0.0,
  freq = 440.0
  }
mkLPF :: LPF -> Sc3.Ugen
mkLPF (LPF r a' b') = Sc3.mkOsc r "LPF" [a',b'] 1
instance Make LPF where
  ugen = mkLPF
