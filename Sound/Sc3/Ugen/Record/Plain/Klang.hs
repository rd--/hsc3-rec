-- | Sine oscillator bank
module Sound.Sc3.Ugen.Record.Plain.Klang where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Klang = Klang {
  rate :: Sc3.Rate,
  freqscale :: Sc3.Ugen,
  freqoffset :: Sc3.Ugen,
  specificationsArrayRef :: Sc3.Ugen
  } deriving (Show)
klangR :: Klang
klangR = Klang {
  rate = Sc3.ar,
  freqscale = 1.0,
  freqoffset = 0.0,
  specificationsArrayRef = 0.0
  }
mkKlang :: Klang -> Sc3.Ugen
mkKlang (Klang r a' b' c') = Sc3.mkOsc r "Klang" [a',b',c'] 1
instance Make Klang where
  ugen = mkKlang
