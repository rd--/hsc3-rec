-- | Exponential single random number generator.
module Sound.Sc3.Ugen.Record.Plain.ExpRand where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data ExpRand = ExpRand {
  rate :: Sc3.Rate,
  lo :: Sc3.Ugen,
  hi :: Sc3.Ugen
  } deriving (Show)
expRandR :: ExpRand
expRandR = ExpRand {
  rate = Sc3.ir,
  lo = 0.01,
  hi = 1.0
  }
mkExpRand :: ExpRand -> Sc3.Ugen
mkExpRand (ExpRand r a' b') = Sc3.mkOsc r "ExpRand" [a',b'] 1
instance Make ExpRand where
  ugen = mkExpRand
