-- | Fast sine oscillator.
module Sound.Sc3.Ugen.Record.Plain.FSinOsc where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data FSinOsc = FSinOsc {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen,
  iphase :: Sc3.Ugen
  } deriving (Show)
fSinOscR :: FSinOsc
fSinOscR = FSinOsc {
  rate = Sc3.ar,
  freq = 440.0,
  iphase = 0.0
  }
mkFSinOsc :: FSinOsc -> Sc3.Ugen
mkFSinOsc (FSinOsc r a' b') = Sc3.mkOsc r "FSinOsc" [a',b'] 1
instance Make FSinOsc where
  ugen = mkFSinOsc
