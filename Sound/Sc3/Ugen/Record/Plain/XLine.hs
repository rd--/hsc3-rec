-- | Exponential line generator.
module Sound.Sc3.Ugen.Record.Plain.XLine where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data XLine = XLine {
  rate :: Sc3.Rate,
  start :: Sc3.Ugen,
  end :: Sc3.Ugen,
  dur :: Sc3.Ugen,
  doneAction :: Sc3.Ugen
  } deriving (Show)
xLineR :: XLine
xLineR = XLine {
  rate = Sc3.ar,
  start = 1.0,
  end = 2.0,
  dur = 1.0,
  doneAction = 0.0
  }
mkXLine :: XLine -> Sc3.Ugen
mkXLine (XLine r a' b' c' d') = Sc3.mkOsc r "XLine" [a',b',c',d'] 1
instance Make XLine where
  ugen = mkXLine
