-- | Schroeder allpass delay line with no interpolation.
module Sound.Sc3.Ugen.Record.Plain.AllpassN where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data AllpassN = AllpassN {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  maxdelaytime :: Sc3.Ugen,
  delaytime :: Sc3.Ugen,
  decaytime :: Sc3.Ugen
  } deriving (Show)
allpassNR :: AllpassN
allpassNR = AllpassN {
  rate = Sc3.ar,
  input = 0.0,
  maxdelaytime = 0.2,
  delaytime = 0.2,
  decaytime = 1.0
  }
mkAllpassN :: AllpassN -> Sc3.Ugen
mkAllpassN (AllpassN r a' b' c' d') = Sc3.mkOsc r "AllpassN" [a',b',c',d'] 1
instance Make AllpassN where
  ugen = mkAllpassN
