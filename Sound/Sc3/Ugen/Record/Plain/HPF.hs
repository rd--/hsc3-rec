-- | 2nd order Butterworth highpass filter.
module Sound.Sc3.Ugen.Record.Plain.HPF where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data HPF = HPF {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  freq :: Sc3.Ugen
  } deriving (Show)
hpfR :: HPF
hpfR = HPF {
  rate = Sc3.ar,
  input = 0.0,
  freq = 440.0
  }
mkHPF :: HPF -> Sc3.Ugen
mkHPF (HPF r a' b') = Sc3.mkOsc r "HPF" [a',b'] 1
instance Make HPF where
  ugen = mkHPF
