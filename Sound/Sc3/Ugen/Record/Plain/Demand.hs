-- | Demand results from demand rate Ugens.
module Sound.Sc3.Ugen.Record.Plain.Demand where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Demand = Demand {
  rate :: Sc3.Rate,
  trig_ :: Sc3.Ugen,
  reset :: Sc3.Ugen,
  demandUgens :: Sc3.Ugen
  } deriving (Show)
demandR :: Demand
demandR = Demand {
  rate = Sc3.ar,
  trig_ = 0.0,
  reset = 0.0,
  demandUgens = 0.0
  }
mkDemand :: Demand -> Sc3.Ugen
mkDemand (Demand r a' b' c') = Sc3.mkOsc r "Demand" [a',b',c'] undefined
instance Make Demand where
  ugen = mkDemand
