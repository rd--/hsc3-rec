-- | Step noise
module Sound.Sc3.Ugen.Record.Plain.LFNoise0 where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data LFNoise0 = LFNoise0 {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen
  } deriving (Show)
lfNoise0R :: LFNoise0
lfNoise0R = LFNoise0 {
  rate = Sc3.ar,
  freq = 500.0
  }
mkLFNoise0 :: LFNoise0 -> Sc3.Ugen
mkLFNoise0 (LFNoise0 r a') = Sc3.mkOsc r "LFNoise0" [a'] 1
instance Make LFNoise0 where
  ugen = mkLFNoise0
