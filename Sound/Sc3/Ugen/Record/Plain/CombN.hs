-- | Comb delay line with no interpolation.
module Sound.Sc3.Ugen.Record.Plain.CombN where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data CombN = CombN {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  maxdelaytime :: Sc3.Ugen,
  delaytime :: Sc3.Ugen,
  decaytime :: Sc3.Ugen
  } deriving (Show)
combNR :: CombN
combNR = CombN {
  rate = Sc3.ar,
  input = 0.0,
  maxdelaytime = 0.2,
  delaytime = 0.2,
  decaytime = 1.0
  }
mkCombN :: CombN -> Sc3.Ugen
mkCombN (CombN r a' b' c' d') = Sc3.mkOsc r "CombN" [a',b',c',d'] 1
instance Make CombN where
  ugen = mkCombN
