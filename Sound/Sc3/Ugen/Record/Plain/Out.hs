-- | Write a signal to a bus.
module Sound.Sc3.Ugen.Record.Plain.Out where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Out = Out {
  rate :: Sc3.Rate,
  bus :: Sc3.Ugen,
  input :: Sc3.Ugen
  } deriving (Show)
outR :: Out
outR = Out {
  rate = Sc3.ar,
  bus = 0.0,
  input = 0.0
  }
mkOut :: Out -> Sc3.Ugen
mkOut (Out r a' b') = Sc3.mkOsc r "Out" [a',b'] 0
instance Make Out where
  ugen = mkOut
