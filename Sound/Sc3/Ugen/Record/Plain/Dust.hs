-- | Random impulses.
module Sound.Sc3.Ugen.Record.Plain.Dust where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Dust = Dust {
  rate :: Sc3.Rate,
  density :: Sc3.Ugen
  } deriving (Show)
dustR :: Dust
dustR = Dust {
  rate = Sc3.ar,
  density = 0.0
  }
mkDust :: Dust -> Sc3.Ugen
mkDust (Dust r a') = Sc3.mkOsc r "Dust" [a'] 1
instance Make Dust where
  ugen = mkDust
