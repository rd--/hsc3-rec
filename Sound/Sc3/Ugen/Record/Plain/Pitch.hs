-- | Autocorrelation pitch follower
module Sound.Sc3.Ugen.Record.Plain.Pitch where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Pitch = Pitch {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  initFreq :: Sc3.Ugen,
  minFreq :: Sc3.Ugen,
  maxFreq :: Sc3.Ugen,
  execFreq :: Sc3.Ugen,
  maxBinsPerOctave :: Sc3.Ugen,
  median :: Sc3.Ugen,
  ampThreshold :: Sc3.Ugen,
  peakThreshold :: Sc3.Ugen,
  downSample :: Sc3.Ugen,
  clar :: Sc3.Ugen
  } deriving (Show)
pitchR :: Pitch
pitchR = Pitch {
  rate = Sc3.kr,
  input = 0.0,
  initFreq = 440.0,
  minFreq = 60.0,
  maxFreq = 4000.0,
  execFreq = 100.0,
  maxBinsPerOctave = 16.0,
  median = 1.0,
  ampThreshold = 0.01,
  peakThreshold = 0.5,
  downSample = 1.0,
  clar = 0.0
  }
mkPitch :: Pitch -> Sc3.Ugen
mkPitch (Pitch r a' b' c' d' e' f' g' h' i' j' k') = Sc3.mkOsc r "Pitch" [a',b',c',d',e',f',g',h',i',j',k'] 2
instance Make Pitch where
  ugen = mkPitch
