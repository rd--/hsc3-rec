-- | Impulse oscillator.
module Sound.Sc3.Ugen.Record.Plain.Impulse where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Impulse = Impulse {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen,
  phase :: Sc3.Ugen
  } deriving (Show)
impulseR :: Impulse
impulseR = Impulse {
  rate = Sc3.ar,
  freq = 440.0,
  phase = 0.0
  }
mkImpulse :: Impulse -> Sc3.Ugen
mkImpulse (Impulse r a' b') = Sc3.mkOsc r "Impulse" [a',b'] 1
instance Make Impulse where
  ugen = mkImpulse
