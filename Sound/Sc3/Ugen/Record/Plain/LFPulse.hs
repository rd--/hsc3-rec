-- | pulse oscillator
module Sound.Sc3.Ugen.Record.Plain.LFPulse where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data LFPulse = LFPulse {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen,
  iphase :: Sc3.Ugen,
  width :: Sc3.Ugen
  } deriving (Show)
lfPulseR :: LFPulse
lfPulseR = LFPulse {
  rate = Sc3.ar,
  freq = 440.0,
  iphase = 0.0,
  width = 0.5
  }
mkLFPulse :: LFPulse -> Sc3.Ugen
mkLFPulse (LFPulse r a' b' c') = Sc3.mkOsc r "LFPulse" [a',b',c'] 1
instance Make LFPulse where
  ugen = mkLFPulse
