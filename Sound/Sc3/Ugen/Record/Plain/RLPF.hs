-- | A resonant low pass filter.
module Sound.Sc3.Ugen.Record.Plain.RLPF where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data RLPF = RLPF {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  freq :: Sc3.Ugen,
  rq :: Sc3.Ugen
  } deriving (Show)
rlpfR :: RLPF
rlpfR = RLPF {
  rate = Sc3.ar,
  input = 0.0,
  freq = 440.0,
  rq = 1.0
  }
mkRLPF :: RLPF -> Sc3.Ugen
mkRLPF (RLPF r a' b' c') = Sc3.mkOsc r "RLPF" [a',b',c'] 1
instance Make RLPF where
  ugen = mkRLPF
