-- | Pulse divider.
module Sound.Sc3.Ugen.Record.Plain.PulseDivider where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data PulseDivider = PulseDivider {
  rate :: Sc3.Rate,
  trig_ :: Sc3.Ugen,
  div_ :: Sc3.Ugen,
  start :: Sc3.Ugen
  } deriving (Show)
pulseDividerR :: PulseDivider
pulseDividerR = PulseDivider {
  rate = Sc3.ar,
  trig_ = 0.0,
  div_ = 2.0,
  start = 0.0
  }
mkPulseDivider :: PulseDivider -> Sc3.Ugen
mkPulseDivider (PulseDivider r a' b' c') = Sc3.mkOsc r "PulseDivider" [a',b',c'] 1
instance Make PulseDivider where
  ugen = mkPulseDivider
