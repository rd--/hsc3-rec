-- | Line generator.
module Sound.Sc3.Ugen.Record.Plain.Line where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Line = Line {
  rate :: Sc3.Rate,
  start :: Sc3.Ugen,
  end :: Sc3.Ugen,
  dur :: Sc3.Ugen,
  doneAction :: Sc3.Ugen
  } deriving (Show)
lineR :: Line
lineR = Line {
  rate = Sc3.ar,
  start = 0.0,
  end = 1.0,
  dur = 1.0,
  doneAction = 0.0
  }
mkLine :: Line -> Sc3.Ugen
mkLine (Line r a' b' c' d') = Sc3.mkOsc r "Line" [a',b',c',d'] 1
instance Make Line where
  ugen = mkLine
