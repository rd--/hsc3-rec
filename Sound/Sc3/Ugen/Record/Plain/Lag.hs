-- | Exponential lag
module Sound.Sc3.Ugen.Record.Plain.Lag where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Lag = Lag {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  lagTime :: Sc3.Ugen
  } deriving (Show)
lagR :: Lag
lagR = Lag {
  rate = Sc3.ar,
  input = 0.0,
  lagTime = 0.1
  }
mkLag :: Lag -> Sc3.Ugen
mkLag (Lag r a' b') = Sc3.mkOsc r "Lag" [a',b'] 1
instance Make Lag where
  ugen = mkLag
