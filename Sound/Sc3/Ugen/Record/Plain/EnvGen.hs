-- | Envelope generator
module Sound.Sc3.Ugen.Record.Plain.EnvGen where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data EnvGen = EnvGen {
  rate :: Sc3.Rate,
  gate_ :: Sc3.Ugen,
  levelScale :: Sc3.Ugen,
  levelBias :: Sc3.Ugen,
  timeScale :: Sc3.Ugen,
  doneAction :: Sc3.Ugen,
  envelope_ :: Sc3.Ugen
  } deriving (Show)
envGenR :: EnvGen
envGenR = EnvGen {
  rate = Sc3.ar,
  gate_ = 1.0,
  levelScale = 1.0,
  levelBias = 0.0,
  timeScale = 1.0,
  doneAction = 0.0,
  envelope_ = 0.0
  }
mkEnvGen :: EnvGen -> Sc3.Ugen
mkEnvGen (EnvGen r a' b' c' d' e' f') = Sc3.mkOsc r "EnvGen" [a',b',c',d',e',f'] 1
instance Make EnvGen where
  ugen = mkEnvGen
