-- | Single random number generator.
module Sound.Sc3.Ugen.Record.Plain.Rand where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Rand = Rand {
  rate :: Sc3.Rate,
  lo :: Sc3.Ugen,
  hi :: Sc3.Ugen
  } deriving (Show)
randR :: Rand
randR = Rand {
  rate = Sc3.ir,
  lo = 0.0,
  hi = 1.0
  }
mkRand :: Rand -> Sc3.Ugen
mkRand (Rand r a' b') = Sc3.mkOsc r "Rand" [a',b'] 1
instance Make Rand where
  ugen = mkRand
