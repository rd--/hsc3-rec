-- | Exponential decay
module Sound.Sc3.Ugen.Record.Plain.Decay where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Decay = Decay {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  decayTime :: Sc3.Ugen
  } deriving (Show)
decayR :: Decay
decayR = Decay {
  rate = Sc3.ar,
  input = 0.0,
  decayTime = 1.0
  }
mkDecay :: Decay -> Sc3.Ugen
mkDecay (Decay r a' b') = Sc3.mkOsc r "Decay" [a',b'] 1
instance Make Decay where
  ugen = mkDecay
