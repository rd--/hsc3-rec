-- | Band limited sawtooth.
module Sound.Sc3.Ugen.Record.Plain.Saw where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Saw = Saw {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen
  } deriving (Show)
sawR :: Saw
sawR = Saw {
  rate = Sc3.ar,
  freq = 440.0
  }
mkSaw :: Saw -> Sc3.Ugen
mkSaw (Saw r a') = Sc3.mkOsc r "Saw" [a'] 1
instance Make Saw where
  ugen = mkSaw
