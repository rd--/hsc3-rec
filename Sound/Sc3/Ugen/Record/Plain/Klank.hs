-- | Bank of resonators
module Sound.Sc3.Ugen.Record.Plain.Klank where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data Klank = Klank {
  rate :: Sc3.Rate,
  input :: Sc3.Ugen,
  freqscale :: Sc3.Ugen,
  freqoffset :: Sc3.Ugen,
  decayscale :: Sc3.Ugen,
  specificationsArrayRef :: Sc3.Ugen
  } deriving (Show)
klankR :: Klank
klankR = Klank {
  rate = Sc3.ar,
  input = 0.0,
  freqscale = 1.0,
  freqoffset = 0.0,
  decayscale = 1.0,
  specificationsArrayRef = 0.0
  }
mkKlank :: Klank -> Sc3.Ugen
mkKlank (Klank r a' b' c' d' e') = Sc3.mkOsc r "Klank" [a',b',c',d',e'] 1
instance Make Klank where
  ugen = mkKlank
