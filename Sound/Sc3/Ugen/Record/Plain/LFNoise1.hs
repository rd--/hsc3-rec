-- | Ramp noise
module Sound.Sc3.Ugen.Record.Plain.LFNoise1 where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data LFNoise1 = LFNoise1 {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen
  } deriving (Show)
lfNoise1R :: LFNoise1
lfNoise1R = LFNoise1 {
  rate = Sc3.ar,
  freq = 500.0
  }
mkLFNoise1 :: LFNoise1 -> Sc3.Ugen
mkLFNoise1 (LFNoise1 r a') = Sc3.mkOsc r "LFNoise1" [a'] 1
instance Make LFNoise1 where
  ugen = mkLFNoise1
