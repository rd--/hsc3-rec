-- | Quadratic noise.
module Sound.Sc3.Ugen.Record.Plain.LFNoise2 where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data LFNoise2 = LFNoise2 {
  rate :: Sc3.Rate,
  freq :: Sc3.Ugen
  } deriving (Show)
lfNoise2R :: LFNoise2
lfNoise2R = LFNoise2 {
  rate = Sc3.ar,
  freq = 500.0
  }
mkLFNoise2 :: LFNoise2 -> Sc3.Ugen
mkLFNoise2 (LFNoise2 r a') = Sc3.mkOsc r "LFNoise2" [a'] 1
instance Make LFNoise2 where
  ugen = mkLFNoise2
