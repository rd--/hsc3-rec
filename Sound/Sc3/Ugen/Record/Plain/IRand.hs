-- | Single integer random number generator.
module Sound.Sc3.Ugen.Record.Plain.IRand where
import qualified Sound.Sc3.Common as Sc3
import qualified Sound.Sc3.Ugen as Sc3
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3
import Sound.Sc3.Ugen.Record.Plain
data IRand = IRand {
  rate :: Sc3.Rate,
  lo :: Sc3.Ugen,
  hi :: Sc3.Ugen
  } deriving (Show)
iRandR :: IRand
iRandR = IRand {
  rate = Sc3.ir,
  lo = 0.0,
  hi = 127.0
  }
mkIRand :: IRand -> Sc3.Ugen
mkIRand (IRand r a' b') = Sc3.mkOsc r "IRand" [a',b'] 1
instance Make IRand where
  ugen = mkIRand
