module Sound.Sc3.Ugen.Record.Cru (module M) where

import Sound.Sc3.Common.Enum as M {- hsc3 -}
import Sound.Sc3.Common.Rate as M {- hsc3 -}
import Sound.Sc3.Ugen.Envelope as M {- hsc3 -}

import Sound.Sc3.Ugen.Record.Cru.Type as M {- hsc3-rec -}
